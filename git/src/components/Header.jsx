import React from 'react'
import ImagemLogoSoma from '../images/logo-soma.svg'

export default (props) => {
   return(
      <div className="generalHeader">
         <div className="imageHeader">
            <img className="light-shadow" src={ImagemLogoSoma} alt="" />
         </div>
            
         <div className="textHeader">
            <h2>Acesse o Sistema</h2>
         </div>
      </div>
   );
}