import React from 'react'

export default (props) => {
   return (
      <div className="generalForms">
         <div className="input-group flex-nowrap inputDiv">
            <input type="text" className="form-control inputCod" id="cod" placeholder="Código"/>
         </div>
            
         <div className="input-group flex-nowrap inputDiv">
            <input type="text" className="form-control inputPass" placeholder="Password"/>
         </div>
            
         <div className="buttonForms">
            <button type="button" className="btn buttonEnter">Entrar</button>
         </div>
      </div>
   );
}