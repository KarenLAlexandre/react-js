import './index.css'
import React from 'react'
import ReactDOM from 'react-dom'
import Forms from './components/Forms.jsx'
import Header from './components/Header.jsx'

ReactDOM.render(
    <div className="row general">
        <div className="col generalLogin">
            <Header></Header>
            <Forms></Forms>
        </div>
    </div>,
    document.getElementById('root')
)